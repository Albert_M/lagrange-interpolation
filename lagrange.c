#include "lagrange.h"
#include <stdlib.h>
#include <string.h>

double lagrange(const point_t *points, uint32_t len, double x_point)
{
    if(!points || !len)
        return 0;
    double res = 0;

    for (uint32_t j = 0; j < len; j++)
    {
        double prod = 1;
        for (uint32_t m = 0; m < len; m++)
        {
            if (j == m)
                continue;
            prod *= (x_point - points[m].x) / (points[j].x - points[m].x);
        }
        res += points[j].y * prod;
    }

    return res;
}

static double add_summ_coef(double oldcoeff, double add, double prod)
{
    return oldcoeff + add / prod;
}

double *lagrange_coeffs(const point_t *points, uint32_t len)
{
    if(!points || !len)
        return 0;
    double *res = (double *)calloc(len, sizeof(double));

    for (uint32_t j = 0; j < len; j++)
    {
        double tmpcoeffs[len];
        memset(tmpcoeffs, 0, len * sizeof(double));
        tmpcoeffs[0] = points[j].y;
        double prod = 1;
        for (uint32_t m = 0; m < len; m++)
        {
            if (j == m)
                continue;
            prod *= points[j].x - points[m].x;
            double precedent = 0;
            for (uint32_t k = 0; k < len; k++)
            {
                // *(X - Xm)
                // Compute the new coefficient of X^i based on
                // the old coefficients of X^(i-1) and X^i
                double newres = (tmpcoeffs[k]) * (-points[m].x) + precedent;
                precedent = tmpcoeffs[k];
                tmpcoeffs[k] = newres;
            }
        }

        for (uint32_t k = 0; k < len; k++)
        {
            res[k] = add_summ_coef(res[k], tmpcoeffs[k], prod);
        }
    }
    return res;
}

double lagrange_table(const double* coeffs, uint32_t len, double point)
{
    if(!coeffs || !len)
        return 0;

    double res = 0;
    double multiplier = 1;

    for (uint32_t k = 0; k < len; k++)
    {
        res += coeffs[k] * multiplier;
        multiplier *= point;
    }
    return res;
}