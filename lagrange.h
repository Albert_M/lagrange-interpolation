#pragma once
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    double x;
    double y;
} point_t;

double lagrange(const point_t *points, uint32_t len, double x_point);

double *lagrange_coeffs(const point_t *points, uint32_t len);

double lagrange_table(const double *coeffs, uint32_t len, double point);

#ifdef __cplusplus
}
#endif
